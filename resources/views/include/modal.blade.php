<!-- Basic modal -->
<div id="modal_default" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Peringatan</h5>
			</div>

			<div class="modal-body">
				<h6 class="text-semibold" id="konten_default"></p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
				<button type="button" class="btn btn-primary" id="ok_model_default">Ya</button>
			</div>
		</div>
	</div>
</div>
<!-- /basic modal -->
<!-- Basic modal -->
<div id="modal_ok" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Informasi</h5>
			</div>

			<div class="modal-body-ok">
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-primari" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>
<!-- /basic modal -->